import React from "react";
import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import ReactPlayer from "react-player";
import { videos } from "../data/data";

const ReactVideoPlayer: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Video Player</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        {videos.map((item) => (
          <IonCard>
            <ReactPlayer
              light={item.thumb}
              controls
              playsinline
              width="100%"
              height="200px"
              url={item.url}
            />
          </IonCard>
        ))}
      </IonContent>
    </IonPage>
  );
};

export default ReactVideoPlayer;
